angular.module('app').component('categoryList', {
    templateUrl: '/categories/category-list.html',
    bindings: {
        categories: '=',
        expenses: '='
    },
    controller: function() {
        this.createNewCategory = function() {
            this.categories.$add({name: this.newCategoryName});
            this.newCategoryName = "";
        }
    }
})