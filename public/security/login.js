angular.module('app').component('login', {
    templateUrl: '/security/login.html',
    bindings: {
        currentAuth: '='
    },
    controller: function($firebaseAuthService, $location) {
        
        this.loggedIn = !!this.currentAuth;
        
        this.anonLogin = function() {
            $firebaseAuthService.$authAnonymously().then(function() {
                $location.path('/home');
            }).catch((function(err) {
                this.errorMessage = err.code;
            }).bind(this));
        };
        
        this.emailLogin = function() {
            $firebaseAuthService.$authWithPassword({
                email: "hello2@gmail.com",
                password: "12345"
            }).then(function() {
                $location.path('/home');
            }).catch((function(err) {
                this.errorMessage = err.code;
            }).bind(this));
        };
        
        this.createUser = function() {
            $firebaseAuthService.$createUser({
                email: "hello2@gmail.com",
                password: "12345"
            }).then(function() {
                this.errorMessage = "User Created";
            }).catch((function(err) {
                this.errorMessage = err.code;
            }).bind(this));
        }
    }
});