// old angularfire
// angular.module('app').constant('FirebaseUrl', 'https://family-table.firebaseio.com/')
//     .service('rootRef', ['FirebaseUrl', Firebase]);

//angularfire 1.2
angular.module('app').config(function($firebaseRefProvider){
    $firebaseRefProvider.registerUrl('https://family-table.firebaseio.com');
});